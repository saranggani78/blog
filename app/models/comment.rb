class Comment < ActiveRecord::Base

	# comment belongs to post
	belongs_to :post

	# validates postid and body
	validates_presence_of :post_id
	validates_presence_of :body
end
