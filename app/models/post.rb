class Post < ActiveRecord::Base
	# post has many comments
	has_many :comments, dependent: :destroy

	# validate title and body
	validates_presence_of :title
	validates_presence_of :body
end
